/*
 * drl_radar.c
 *
 *  Created on: 30.10.2018
 *      Author: daniel
 */




#include <DRL/drl_radar.h>
#include <HAL/hal_general.h>
extern USCIB1_SPICom spi_control;

void test(void){
    //test SSI
    uint8_t test_data[5] = {0xFF,0xAA,0xFF,0xAA,0xFF};
    uint8_t *data = test_data;

    spi_control.TxData.len = 5;
    uint8_t i=0;
    for(i = 0; i < spi_control.TxData.len; i++)
    {
        spi_control.TxData.Data[i] = *data++;
    }
    while(1){
        HAL_USCIB1_Transmit();
    }
}

void drl_radar_transmit_ramp(void)
{
    spi_control.TxData.len =1;
    uint16_t upper_bound = 4096;
    uint16_t lower_bound = 0x00;

    uint16_t ramp_data;
    uint16_t i = 0;

    for(i=lower_bound;i<upper_bound;i+=100){

        ramp_data = i<<2;

        spi_control.TxData.Data[0] = ramp_data; //HI byte
        HAL_USCIB1_Transmit();

    }
    spi_control.TxData.Data[0]=0;
    HAL_USCIB1_Transmit();




}
