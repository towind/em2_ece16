/*
 * drl_lcd.h
 *
 *  Created on: 14.11.2016
 *      Author: daniel
 */

#ifndef DRL_DRL_LCD_H_
#define DRL_DRL_LCD_H_

#include <HAL/hal_ssi.h>
#include <HAL/hal_gpio.h>

// definitions
#define LCD_RST				0xD2 //see datasheet
#define LCD_BIAS			0xA3
#define ADC_SEL_NORMAL		0xA0
#define COMMON_REVERSE		0xC9
#define RES_RATIO			0x24
#define ELE_VOL_MODE		0x81
#define ELE_VOL_VALUE		0x12
#define POWER_CONT			0x2F
#define DISPLAY_ON			0xAF
#define DISPLAY_ALL_POINTS	0xA5
#define DISPLAY_ALL_POINTS_OFF	0xA4
#define RST_TOPLEFT			0x40

#define REVERSE 0xA7

#define DISPLAYWIDTH 132
#define DISPLAYHEIGHT 64
#define RAM_PAGES DISPLAYHEIGHT/8

#define CHAR_WIDTH 6 //width of one character on display (with one px space behind character)



void DRL_LCD_WriteCommand (unsigned char *data, unsigned char len);
void DRL_LCD_Init(void);
void DRL_LCD_SetPosition(unsigned char page, unsigned char col);
void DRL_LCD_Clear(void);
void DRL_LCD_ClearPage(unsigned char page);
void DRL_LCD_WriteString(unsigned char str[], unsigned char len, unsigned char page, unsigned char col);
void DRL_LCD_WriteUInt(unsigned int num, unsigned char page, unsigned char len);
void DRL_LCD_WriteUIntwLabel(unsigned char *str, unsigned char labelLen,unsigned int num, unsigned char page, unsigned char col);

//test functions
void DRL_LCD_WriteData(unsigned char *data, unsigned char len);
void DRL_LCD_WriteAry(unsigned char (*ary)[9]); //utterly useless
void DRL_LCD_WriteA(void); //basic test function

void itoa(unsigned int value, char* result, unsigned char base); //ansi standard, source:

#endif /* DRL_DRL_LCD_H_ */
