/*
 * hal_general.h
 *
 *  Created on: 14.03.2017
 *      Author: daniel
 */

#ifndef HAL_GENERAL_H_
#define HAL_GENERAL_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"


#include <HAL/hal_ucs.h>
#include <HAL/hal_gpio.h>
#include <HAL/hal_ssi.h>
#include <HAL/hal_pwm.h>
#include <HAL/hal_uart.h>


void HAL_Init();

#endif /* HAL_GENERAL_H_ */
