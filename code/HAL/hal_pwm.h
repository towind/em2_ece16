#include "driverlib/pin_map.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"


#include <HAL/hal_gpio.h>

#define PWM_STEERING PWM_OUT_2
#define PWM_THROTTLE PWM_OUT_3

#define PWM_FREQ 60
void HAL_PWM_Init(void);

void HAL_PWMSetSteering(int val);

void HAL_PWMSetThrottle(int val);

int HAL_PWMThrottleSetupPulses(int val);
