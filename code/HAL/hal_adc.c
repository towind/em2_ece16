/*
 * hal_adc.c
 *
 *  Created on: 27.04.2018
 *      Author: Dominic Langbauer
 */

#include "../tiva_headers.h"
#include "DRIVER/driver_headers.h"
#include "hal_headers.h"

void HAL_ADC_INIT()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)); // Wait for Port to be ready

    //Pin Type ADC
    GPIOPinTypeADC(GPIO_PORTE_BASE, RADAR_IF1);
    GPIOPinTypeADC(GPIO_PORTE_BASE, RADAR_IF2);
    GPIOPinTypeADC(GPIO_PORTE_BASE, IR1_SENSE_OUT);
    GPIOPinTypeADC(GPIO_PORTD_BASE, IR2_SENSE_OUT);
    GPIOPinTypeADC(GPIO_PORTD_BASE, IR3_SENSE_OUT);

    // 3V reference Voltage einstellen
    ADCReferenceSet(ADC0_BASE, ADC_REF_INT);

    // Disable ADC
    ADCSequenceDisable(ADC0_BASE,0);

    ADCSequenceConfigure(ADC0_BASE, 0,ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 0,0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC0_BASE, 0,1, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE, 0,2, ADC_CTL_CH2);
    ADCSequenceStepConfigure(ADC0_BASE, 0,3, ADC_CTL_CH3);
    ADCSequenceStepConfigure(ADC0_BASE, 0,4, ADC_CTL_CH4 | ADC_CTL_END);

    // Enable ADC
    ADCSequenceEnable(ADC0_BASE,0);
}

void SAMPLE_ADC(uint32_t data[5])
{
    ADCProcessorTrigger(ADC0_BASE , 0);
    while (ADC0_ACTSS_R & ADC_ACTSS_BUSY); // busy
    ADCIntClear (ADC0_BASE, 0);
    ADCSequenceDataGet (ADC0_BASE, 0, data);
}

