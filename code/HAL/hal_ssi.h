#ifndef HAL_HAL_SSI_H_
#define HAL_HAL_SSI_H_


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"

#include <HAL/hal_gpio.h>

#define SPI_CLK_FREQ 100000 //spi clock frequency 100 kHz

void HAL_USCIB1_Init();
void HAL_USCIB1_Transmit();



typedef struct
{
	union
	{
		unsigned char R;
		struct
		{
			unsigned char TxSuc:1; // Bit=1 wenn Daten uebertragen wurden
			unsigned char dummy:7;
		}B;
	}Status;

	struct
	{
		unsigned int len; // Länge der Daten in Bytes die übertragen werden
		unsigned int cnt;// Index auf momentan übertragene Daten
		unsigned int Data[4096];// Tx Daten Array
	}TxData;


	struct
	{
		unsigned char len; // Laenge der empfangenen Daten
		unsigned int Data[200]; // Rx Daten Array
	}RxData;

}USCIB1_SPICom;

#endif
