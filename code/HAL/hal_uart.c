/*
 * hal_uart.c
 *
 *  Created on: Mar 21, 2017
 *      Author: daniel
 */

#include <HAL/hal_uart.h>

//USAGE: UARTprintf("SSI ->\n"); UARTprintf("'%c' ", c);

void HAL_InitUart()
{
	// Configure the pin muxing for UART0 functions on port A0 and A1.
	// This step is not necessary if your part does not support pin muxing.
	// TODO: change this to select the port/pin you are using.
	//
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);
	// Enable UART0 so that we can configure the clock.
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

	//
	// Use the internal 16MHz oscillator as the UART clock source.
	//
	UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

	//
	// Select the alternate (UART) function for these pins.
	// TODO: change this to select the port/pin you are using.
	//
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	//
	// Initialize the UART for console I/O.
	//
	UARTStdioConfig(0, 115200, SysCtlClockGet());
}


