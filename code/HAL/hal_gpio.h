/*
 * hal_gpio.h
 *
 *  Created on: 14. M�rz 2017
 *      Author: schlagba15
 */

#ifndef HAL_HAL_GPIO_H_
#define HAL_HAL_GPIO_H_

#include "driverlib/pin_map.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"




// defines for J1

#define RADAR_SPI_CS	GPIO_PIN_5
#define US1_SIGNAL_OUT	GPIO_PIN_0
#define US2_SIGNAL_OUT	GPIO_PIN_1
#define US2_SIGNAL_IN	GPIO_PIN_4
#define US1_SIGNAL_IN	GPIO_PIN_5
#define RADAR_SPI_SCLK	GPIO_PIN_4
#define US2_DRIVER_EN	GPIO_PIN_5
#define STEERING		GPIO_PIN_6
#define THROTTLE		GPIO_PIN_7
// defines for J2
#define MOTION_I2C_SCL	GPIO_PIN_2
#define VBAT_MEASURE	GPIO_PIN_0
#define MOTION_INT		GPIO_PIN_0
#define NC_0			GPIO_PIN_7
#define NC_1			GPIO_PIN_6
#define LCD_RESET		GPIO_PIN_4
#define LCD_A0			GPIO_PIN_3
#define LCD_BACKLIGHT	GPIO_PIN_2
// defines for J3
#define US1_DRIVER_EN	GPIO_PIN_0
#define RADAR_SPI_MOSI	GPIO_PIN_1
#define IR3_SENSE_OUT	GPIO_PIN_2
#define IR2_SENSE_OUT	GPIO_PIN_3
#define IR1_SENSE_OUT	GPIO_PIN_1
#define RADAR_IF1		GPIO_PIN_2
#define RADAR_IF2		GPIO_PIN_3
#define LCD_SPI_MOSI	GPIO_PIN_1
// defines for J4
#define LCD_SPI_SCLK	GPIO_PIN_2
#define LCD_SPI_CS		GPIO_PIN_3
#define MOTION_I2C_SDA	GPIO_PIN_3
#define RF_TXD			GPIO_PIN_4
#define RF_RXD			GPIO_PIN_5
#define UNUSED_6		GPIO_PIN_6
#define UNUSED_7		GPIO_PIN_7
#define REMOTE_CH1		GPIO_PIN_6
#define REMOTE_CH2		GPIO_PIN_7
#define UNUSED_4 		GPIO_PIN_4

// Funktionsprototypen

void HAL_GPIO_Init();
void delayMS(int ms); //practical but wasteful

//makros for drl lcd

#define LCD_BACKLIGHT_ON GPIOPinWrite(GPIO_PORTA_BASE,LCD_BACKLIGHT,LCD_BACKLIGHT)
#define LCD_BACKLIGHT_OFF GPIOPinWrite(GPIO_PORTA_BASE,LCD_BACKLIGHT,0x00)
#define LCD_RESET_HI	GPIOPinWrite(GPIO_PORTA_BASE,LCD_RESET,LCD_RESET)
#define LCD_RESET_LO	GPIOPinWrite(GPIO_PORTA_BASE,LCD_RESET,0x00)
#define LCD_DATACMD_HI	GPIOPinWrite(GPIO_PORTA_BASE,LCD_A0,LCD_A0)
#define LCD_DATACMD_LO	GPIOPinWrite(GPIO_PORTA_BASE,LCD_A0,0x00)
#define LCD_CS_HI		GPIOPinWrite(GPIO_PORTF_BASE,LCD_SPI_CS,LCD_SPI_CS)
#define LCD_CS_LO		GPIOPinWrite(GPIO_PORTF_BASE,LCD_SPI_CS,0x00)

#endif /* HAL_HAL_GPIO_H_ */
