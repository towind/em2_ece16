/*
 * hal_ucs.c
 *
 *  Created on: 14.03.2017
 *      Author: daniel
 */

#include <HAL/hal_ucs.h>

void HAL_UCS_Init()
{
	SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC |   SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
}

