/*
 * radix.c
 *
 *  Created on: 22.10.2018
 *      Author: daniel
 */




#include "radix.h"
#include "twiddles.h"

void calc_fft(float* re, float*im, short len){
    int stage=0,Half=0,i=0,n=0,pos=0,k=0;

    float w_real,w_imag,realPathTemp,imagPathTemp;

    //reverse bit order

    Scramble_data(re, im,512);

    float nSamples = 512;

    int Order = 9; //fixed order for fixed sample count

    Half = 1;


    for(stage = 1; stage<=Order;stage++){ //Order loop (from 1 to 9)

        for(i=0;i<nSamples;i = i+pow(2,stage)){
            //for n = 0:(Half-1)
            for(n=0;n<Half;n++){
                pos = n+i;
                k = pow(2,(Order-stage))*n;


                //%real(w);
                w_real = twiddler_real[k];//cos((2*pi)*k/nSamples); //todo use twiddle lookup table
                //%imag(w);
                w_imag = twiddler_imag[k];//-sin((2*pi)*k/nSamples); //todo use twiddle lookup table

                //multiplikate with twiddles
                //realPathTemp = x_real(pos+Half)*w_real - x_imag(pos+Half)*w_imag;
                realPathTemp = re[pos+Half]*w_real - im[pos+Half]*w_imag;
                //imagPathTemp = x_imag(pos+Half)*w_real + x_real(pos+Half)*w_imag;
                imagPathTemp = im[pos+Half]*w_real + re[pos+Half]*w_imag;



                //x_real(pos+Half) = x_real(pos) - realPathTemp;
                re[pos+Half] = re[pos] - realPathTemp;
                //x_imag(pos+Half) = x_imag(pos) - imagPathTemp;
                im[pos+Half] = im[pos] - imagPathTemp;

                //x_real(pos) = x_real(pos) + realPathTemp;
                re[pos] = re[pos] + realPathTemp;
                //x_imag(pos) = x_imag(pos) + imagPathTemp;
                im[pos] = im[pos] + imagPathTemp;


            }
        }
        Half=2*Half;
    }

}

//tools for the fft

//reorganize array to get a non-scrambled output signal
void Scramble_data(float* re, float*im, short len){

    float temp_re; // temporary storage complex variable swaps
    float temp_im;
    short i; // current sample index
    short j; // bit reversed index
    short k; // used to propagate carryovers

    short N2 = len/2; // N2 = N >> 1

    // Bit-reversing algorithm. Since 0 -> 0 and N-1 -> N-1
    // under bit-reversal,these two reversals are skipped.

    j = 0;
    for(i=1; i<(len-1); i++)
    {
        k = N2; // k is 1 in msb, 0 elsewhere
        while(k<=j) // Propagate carry to the right if bit is 1
        {
            j = j - k; // Bit tested is 1, so clear it.
            k = k>>1; // Carryover binary 1to right one bit.
        }
        j = j+k; // current bit tested is 0, add 1 to that bit
        // Swap samples if current index is less than bit reversed index.
        if(i<j)
        {
            temp_re=re[j];
            temp_im=im[j];

            re[j]=re[i];
            im[j]=im[i];

            re[i]=temp_re;
            im[i]=temp_im;
        }
    }
}


