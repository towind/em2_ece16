/*
 * radix.h
 *
 *  Created on: 22.10.2018
 *      Author: daniel
 */

#ifndef AL_RADIX_H_
#define AL_RADIX_H_

#include <math.h>
//math define
#define pi 3.1416


void calc_fft(float* re, float*im, short len);

//tools
void Scramble_data(float* re, float*im, short len);




#endif /* AL_RADIX_H_ */
